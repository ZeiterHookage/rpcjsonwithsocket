const jwt = require('jsonwebtoken');
const speakeasy = require('speakeasy');
const { io } = require('../JsonRpc');
//check tokens
let verifyToken = (req, res, next) => {
    let token = req.get('Authorization');
    jwt.verify(token, process.env.SEED, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                ok: false,
                err: {
                    message: 'The Token is not valid'
                }
            })
        }
        req.user = decoded.user;
        next();
    })
};



// check roles // checks it
let verifyRole = (req, res, next) => {
    let user = req.user;
    if (user.role === 'ADMIN_ROLE') {
        next();
    } else {
        return res.json({
            ok: false,
            err: 'the User is not an Admin'
        })
    }
}


let Verify2FA = (req, res, next) => {

    let token = {};
    let secrect = {};
    io.on('connection', (Client) => {

        Client.on('qrcode', (token) => {
            token = token;
            console.log('token', token);

        });

        Client.on('secrect', (secrect) => {
            secrect = secrect;
            console.log('Secrect', secrect);
        })

    })

    next();
    /*if (!token === undefined) {
        
    } else {
        res.status(400).json({
            ok: false,
            msg: 'musst send the token'
        })
    }*/

};

let VerifyCaptcha = (req, res, next) => {

    let token = {};
    let secrect = {};
    io.on('connection', (Client) => {

        Client.on('captcha', (captcha) => {

            console.log('captcha', captcha);

        });


    })

    next();
}


module.exports = {
    verifyToken,
    verifyRole,
    Verify2FA,
    VerifyCaptcha
}