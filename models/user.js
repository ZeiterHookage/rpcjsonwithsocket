const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');


let Schema = mongoose.Schema;

let ValidRolle = {
    values: ['ADMIN_ROLE', 'USER_ROLE'],
    message: '{VALUE} That ist it not a valid Role x_X :('
}

let UserSchema = new Schema({
    name: {
        type: String,
        required: [true, 'the name is mandatory']

    },
    lastname: {
        type: String,
        required: [true, 'the last name is mandatory']

    },
    password: {
        type: String,
        required: [true, 'the password is mandatory']
    },
    email: {
        type: String,
        unique: true,
        required: [true, 'the email is mandatory']
    },
    img: {
        type: String,
        required: false
    },
    role: {
        type: String,
        default: 'USER_ROLE',
        enum: ValidRolle

    },
    state: {
        type: Boolean,
        default: false
    },
    google: {
        type: Boolean,
        default: false
    }
});
UserSchema.methods.toJSON = function() {
    let user = this;
    let userObject = user.toObject();
    delete userObject.password;

    return userObject;
}
UserSchema.plugin(uniqueValidator, { message: '{PATH} hould be unique' });

module.exports = mongoose.model('UserSch', UserSchema);