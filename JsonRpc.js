//entviroment variable
require('./config/config');

const axios = require('axios');
const express = require('express');
const socketIO = require('socket.io');
const http = require('http');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');



//epress init
const app = express();

//implementing socket 
let server = http.createServer(app);
module.exports.io = socketIO(server);
require('./socketServer');
require('./middlewares/Authentication');


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//cors
app.use(cors());

// enable public folder
app.use(express.static(path.resolve(__dirname, '../public')));

let data = [];




//routes
app.use(require('./routes/index'));


//Axios defaults configuration and headers
/*axios.defaults.baseURL = 'http://192.168.1.15:9332';
axios.defaults.headers.post['Content-Type'] = 'appication/json';
var options = {
    method: 'POST',
    headers: {
        'cache-control': 'no-cache',
        Connection: 'keep-alive',
        'Content-Length': '96',
        'Accept-Encoding': 'gzip, deflate',
        Host: '192.168.1.15:9332',
        'Cache-Control': 'no-cache',
       // Accept: '**',
        //'User-Agent': 'PostmanRuntime/7.19.0',
        Authorization: 'Basic SGFtdW5kTmljOkhhbWxldC8wOTA1ODQ='
    },
    data: '{\n    "jsonrpc": "1.0",\n    "id": "curltest",\n    "method": "getblockchaininfo",\n    "params": []\n}'
};*/

//server request to Litecoin
/*axios.request(options)
    .then(resp => {
        console.log(resp.data)
        this.data = resp.data
            //localhost:3000 website
            // app.get('/', (req, res) => res.send(resp.data));
    })
    .catch(err => {
        console.log('errorrrr', err)
    })*/

//app.get('/', (req, res) => res.send('hello'));
// database MongodB ArawakEx
mongoose.connect('mongodb://localhost:27017/ArawaksEX', function(err, resp) {
    if (err) throw err;

    console.log("ArawaksEx is alive!!!!");

});
//port listener to 3000 with express 
server.listen(process.env.PORT, () => console.log(`listening on port ${process.env.PORT}!`))