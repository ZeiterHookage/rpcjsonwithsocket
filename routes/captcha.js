const express = require('express');
const request = require('request');

const app = express();

app.get('/', (req, res) => {
    // Sending our HTML file to browser.
    console.log('hello--->', req.body);
    res.sendFile(__dirname + '/index.html');
});

app.post('/subscribe', (req, res) => {
    //if captcha is null,empty or undefined
    if (req.body.captcha === undefined ||
        req.body.captcha === '' ||
        req.body.captcha === null
    ) {
        return res.status(400).json({ "success": false, "message": "Please select captcha" });

    }
    // Put your secret key here.
    const secretKey = "6LfducAUAAAAAE9YLp51YbPc-ss-6zNl7i5lK8tU";
    // req.connection.remoteAddress will provide IP address of connected user.
    const verificationUrl = `https://google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${req.body.captcha}&remoteip=${req.connection.remoteAddress}`;


    // Hitting GET request to the URL, Google will respond with success or error scenario.
    request(verificationUrl, (err, response, body) => {
        body = JSON.parse(body);
        //console.log('body-->', body);

        // Success will be true or false depending upon captcha validation.
        if (body.ok !== undefined && !body.ok) {
            return res.status(400).json({ "success": false, "message": "Failed  captcha verification" });
        }
        // if successfull
        return res.json({ "success": true, "message": "captcha is successfull" });
        console.log('succesfull', { "success": true, "message": "captcha is successfull" });
    });
});

// This will handle 404 requests.
app.use("*", function(req, res) {
    res.status(404).send("404");
})

module.exports = app;