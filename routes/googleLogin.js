const express = require('express');
const jwt = require('jsonwebtoken');
const Users = require('../../models/user');
const { OAuth2Client } = require('google-auth-library');
const client = new OAuth2Client(process.env.CLIENT_ID);
const app = express();
//google configuration

async function verify(token) {
    const ticket = await client.verifyIdToken({
        idToken: token,
        audience: process.env.CLIENT_ID, // Specify the CLIENT_ID of the app that accesses the backend
        // Or, if multiple clients access the backend:
        //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
    });
    const payload = ticket.getPayload();
    //const userid = payload['sub'];
    // If request specified a G Suite domain:
    //const domain = payload['hd'];
    console.log(payload.name);
    console.log(payload.email);
    console.log(payload.picture);
    return {
        name: payload.name,
        email: payload.email,
        img: payload.picture,
        google: true
    }
}

//google SignIn route
app.post('/google', async(req, res) => {
    let token = req.body.idtoken;
    let googleUser = await verify(token)
        .catch(e => {
            return res.status(403).json({
                ok: false,
                err: e
            })
        })
    Users.findOne({ email: googleUser.email }, (err, UsersDB) => {

        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });

        };

        if (UsersDB) {

            if (UsersDB.google === false) {
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'Musst login as normal user'
                    }
                });

            } else {
                let token = jwt.sign({
                    user: UsersDB
                }, process.env.SEED, { expiresIn: process.env.EXPIRY_TOKEN });

                return res.json({
                    ok: true,
                    user: UsersDB,
                    token
                })
            }



        } else {

            // First time new User

            let user = new Users();
            let Nname = googleUser.name.split(" ");
            console.log("lastname", lastN);
            user.name = Nname[0],
                user.lastname = Nname[1];
            user.email = googleUser.email,
                user.img = googleUser.img,
                user.google = true,
                user.password = ':)';

            user.save((err, userDB) => {
                if (err) {
                    return res.status(500).json({
                        ok: false,
                        err
                    });

                };

                let token = jwt.sign({
                    user: UsersDB
                }, process.env.SEED, { expiresIn: process.env.EXPIRY_TOKEN });

                return res.json({
                    ok: true,
                    user: UsersDB,
                    token
                })

            })
        }
    })

});



module.exports = app;