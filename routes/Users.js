const express = require('express');
const app = express();
const Users = require('../../models/user');
const { verifyToken, verifyRole } = require('../middlewares/Authentication');
const bcrypt = require('bcrypt');
const _ = require('underscore');



// get request form Mongodb
app.get('/users', verifyToken, (req, res) => {

    let from = req.query.from || 0;
    from = Number(from);

    let limit = req.query.limit || 5;
    limit = Number(limit);
    //sending {state:true} inside find search request for active state 
    Users.find({}) // adding -->'name'   make exlution of all except the params inside the backtips
        .skip(from) // skip the las 5 and show the next 
        .limit(limit) // add limits to 5 response
        .exec((err, users) => {
            if (err) {
                res.status(400).json({
                    ok: true,
                    err
                });

            }
            Users.count({}, (err, counts) => {
                res.json({
                    ok: true,
                    users,
                    Pcount: counts
                })

            })


        })

});

// post request from Mongodb
app.post('/users', [verifyToken, verifyRole], (req, res) => {
    let body = req.body;

    let users = new Users({
        name: body.name,
        lastname: body.lastname,
        password: bcrypt.hashSync(body.password, 10),
        email: body.email,
        role: body.role

    });


    users.save((err, usersDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err


            });

        }
        //usersDB.password = null;

        res.json({
            ok: true,
            user: usersDB
        });
    });


});

// update request from mongodb
app.put('/users/:id', [verifyToken, verifyRole], (req, res) => {
    let id = req.params.id;
    let body = _.pick(req.body, ['name', 'lastname', 'img', 'role', 'state']);


    Users.findByIdAndUpdate(id, body, { new: true, runValidators: true }, (err, usersDB) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err


            });

        }


        res.json({
            ok: true,
            user: usersDB

        });



    })

});

// delete request from mongodb
app.delete('/users/:id', [verifyToken, verifyRole], (req, res) => {
    let id = req.params.id;

    let StateChange = {
        state: false
    };


    // delet complet form dataBase--> //Users.findByIdAndRemove(id, (err, UserDelete) => {

    //make a update of datebase
    Users.findByIdAndUpdate(id, StateChange, { new: true }, (err, UserDelete) => {



        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });

        }
        if (!UserDelete) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'User not found '
                }
            });
        }
        res.json({
            ok: true,
            user: UserDelete

        })

    })



})
module.exports = app;