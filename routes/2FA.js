const express = require('express');
const speakeasy = require('speakeasy');
const QRCode = require('qrcode');
const bodyParser = require('body-parser');




const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


app.post('/2fa', (req, res) => {
    const secret = speakeasy.generateSecret({ length: 20 });

    // console.log(token);
    QRCode.toDataURL(secret.otpauth_url, function(err, image_data) {
        // A data URI for the QR code image
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            })
        }


        return res.json({
            secret: "",
            message: '2FA Auth needs to be verified',
            tempSecret: secret.base32,
            image_data,
            tfaURL: secret.otpauth_url

        });

    });



});



module.exports = app;