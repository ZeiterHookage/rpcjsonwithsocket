const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { Verify2FA, VerifyCaptcha } = require('../middlewares/Authentication')
const Users = require('../../models/user');

const app = express();

app.post('/login', [Verify2FA, VerifyCaptcha], (req, res) => {
    let body = req.body;

    Users.findOne({ email: body.email }, (err, usersDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        };


        if (!usersDB) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: '(User) or password are wrong'
                }
            });
        };


        if (!bcrypt.compareSync(body.password, usersDB.password)) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: '(User) or password are wrong'
                }
            });

        };

        let token = jwt.sign({
            user: usersDB
        }, process.env.SEED, { expiresIn: process.env.EXPIRY_TOKEN })
        res.json({
            ok: true,
            user: usersDB,
            token: token

        })
    })

})



module.exports = app;