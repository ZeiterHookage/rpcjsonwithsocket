const express = require('express');
const app = express();

app.use(require('./googleLogin'));
app.use(require('./Users'));
app.use(require('./login'));
app.use(require('./2FA'));
app.use(require('./captcha'));



module.exports = app;